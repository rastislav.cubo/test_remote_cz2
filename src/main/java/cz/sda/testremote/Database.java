package cz.sda.testremote;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class Database {
    private final Map<UUID, Person> people = new HashMap<>();

    public void save(Person p) {
        people.put(p.getId(), p);
    }

    public Optional<Person> find(UUID id) {
        return Optional.ofNullable(people.get(id));
    }

    public List<Person> find(String name) {
        return people.values().stream().filter(p -> p.getName().equals(name)).collect(Collectors.toList());
    }
}
