package cz.sda.testremote;

import java.util.Scanner;
import java.util.UUID;

public class Main {

    public static void main(String[] args) {
        PeopleService service = new PeopleService(new Database());
        Scanner scanner = new Scanner(System.in);
        System.out.print("$ ");
        String input = scanner.nextLine();
        while (!"exit".equalsIgnoreCase(input)) {
            switch (input) {
                case "save":
                    System.out.print("Please enter name: ");
                    String name = scanner.nextLine();
                    System.out.print("Please enter family name: ");
                    String familyName = scanner.nextLine();
                    System.out.print("Please enter address: ");
                    String address = scanner.nextLine();
                    UUID newId = service.save(name, familyName, address);
                    System.out.println("Saved new person under id " + newId);
                    break;
                case "findById":
                    System.out.print("Please enter id: ");
                    String id = scanner.nextLine();
                    try {
                        UUID uuid = UUID.fromString(id);
                        service.findById(uuid).ifPresentOrElse(
                                System.out::println,
                                () -> System.out.println("No person with id " + uuid + " found"));
                    } catch (IllegalArgumentException e) {
                        System.out.println("Entered id " + id + " is not a valid UUID");
                    }
                    break;
                case "findByName":
                    System.out.print("Please enter name: ");
                    String query = scanner.nextLine();
                    System.out.println(service.findByName(query));
                    break;
                default:
                    System.out.println("unknown command");
            }
            System.out.print("$ ");
            input = scanner.nextLine();
        }
    }
}
