package cz.sda.testremote;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class PeopleService {
    private final Database database;

    public PeopleService(Database database) {
        this.database = database;
    }

    public UUID save(String name, String familyName, String address) {
        Person person = new Person(name, familyName, address);
        database.save(person);
        return person.getId();
    }

    public Optional<Person> findById(UUID id) {
        return database.find(id);
    }

    public List<Person> findByName(String name) {
        return database.find(name);
    }
}
