package cz.sda.testremote;

import java.util.Objects;
import java.util.UUID;

public class Person {
    private UUID id;
    private String name;
    private String familyName;
    private String address;

    public Person(String name, String familyName, String address) {
        this(UUID.randomUUID(), name, familyName, address);
    }

    public Person(UUID id, String name, String familyName, String address) {
        this.id = id;
        this.name = name;
        this.familyName = familyName;
        this.address = address;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(id, person.id) && Objects.equals(name, person.name) && Objects.equals(familyName, person.familyName) && Objects.equals(address, person.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, familyName, address);
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", familyName='" + familyName + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
