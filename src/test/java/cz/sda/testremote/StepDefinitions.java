package cz.sda.testremote;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class StepDefinitions {

    private Database database = new Database();
    private PeopleService service = new PeopleService(database);
    private UUID id;
    private Optional<Person> person;

    @Given("There exists a person with ID {word}")
    public void thereExistsPerson(String uuid) {
        Person p = new Person(UUID.fromString(uuid), null, null, null);
        database.save(p);
    }

    @When("I look up a person by ID {word}")
    public void iLookUpPerson(String uuid) {
        person = service.findById(UUID.fromString(uuid));
    }

    @When("I save person with name {word} family name {word} and address {string}")
    public void iSavePerson(String name, String familyName, String address) {
        id = service.save(name, familyName, address);
    }

    @Then("I should find the person")
    public void iFindPerson() {
        assertThat(person).isNotEmpty();
    }

    @Then("I should not find the person")
    public void iDoNotFindPerson() {
        assertThat(person).isEmpty();
    }

    @Then("There should be person with name {word} {word} in database under new id")
    public void thereShouldBePerson(String name, String familyName) {
        assertThat(database.find(id))
                .isNotEmpty()
                .hasValueSatisfying(p -> {
                    assertThat(p.getName()).isEqualTo(name);
                    assertThat(p.getFamilyName()).isEqualTo(familyName);
                });
    }

}
