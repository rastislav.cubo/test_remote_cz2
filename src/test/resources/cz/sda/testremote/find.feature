Feature: Searching

  Scenario: Find existing person by ID
    Given There exists a person with ID d14c285f-0a0a-4256-abbf-94aa4a3897a2
    When I look up a person by ID d14c285f-0a0a-4256-abbf-94aa4a3897a2
    Then I should find the person

  Scenario: Find not existing person by ID
    When I look up a person by ID d14c285f-0a0a-4256-abbf-94aa4a3897a2
    Then I should not find the person